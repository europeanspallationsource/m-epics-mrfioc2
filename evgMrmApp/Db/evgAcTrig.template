record(longout, "$(SYS)-$(DEVICE):AcTrig-Divider-SP") {
    field( DESC, "EVG AC Divider")
    field( DTYP, "Obj Prop uint32")
    field( OUT,  "@OBJ=$(DEVICE):AcTrig, PROP=Divider")
    field( PINI, "YES")
    field( VAL,  "$(AcTrig-Divider-SP\=0)")
    field( UDF,  "0")
    field( HIHI, "256")
    field( LOLO, "-1")
    field( HHSV, "MAJOR")
    field( LLSV, "MAJOR")
    field( FLNK, "$(SYS)-$(DEVICE):AcTrig-Divider-RB")
    info( autosaveFields_pass0, "VAL")
}

record(longin, "$(SYS)-$(DEVICE):AcTrig-Divider-RB") {
    field( DESC, "EVG AC Divider")
    field( DTYP, "Obj Prop uint32")
    field( INP,  "@OBJ=$(DEVICE):AcTrig, PROP=Divider")
    field( HIHI, "256")
    field( LOLO, "-1")
    field( HHSV, "MAJOR")
    field( LLSV, "MAJOR")
}

record(ao, "$(SYS)-$(DEVICE):AcTrig-Phase-SP") {
    field( DESC, "EVG AC Phase Shifter")
    field( DTYP, "Obj Prop double")
    field( OUT,  "@OBJ=$(DEVICE):AcTrig, PROP=Phase")
    field( PINI, "YES")
    field( VAL,  "$(AcTrig-Phase-SP\=0.0)")
    field( UDF,  "0")
    field( HIHI, "25.6")
    field( LOLO, "-0.1")
    field( HHSV, "MAJOR")
    field( LLSV, "MAJOR")
    field( EGU,  "msec")
    field( HOPR, "25.5")
    field( LOPR, "0")
    field( DRVH, "25.5")
    field( DRVL, "0")
    field( PREC, "1")
    field( FLNK, "$(SYS)-$(DEVICE):AcTrig-Phase-RB")
    info( autosaveFields_pass0, "VAL")
}

record(ai, "$(SYS)-$(DEVICE):AcTrig-Phase-RB") {
    field( DESC, "EVG AC Phase Shifter")
    field( DTYP, "Obj Prop double")
    field( INP,  "@OBJ=$(DEVICE):AcTrig, PROP=Phase")
    field( HIHI, "25.6")
    field( LOLO, "-0.1")
    field( HHSV, "MAJOR")
    field( LLSV, "MAJOR")
    field( EGU,  "msec")
    field( PREC, "1")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-Bypass-Sel") {
    field( DESC, "Bypass AC divider and Phase shifter")
    field( DTYP, "Obj Prop bool")
    field( OUT,  "@OBJ=$(DEVICE):AcTrig, PROP=Bypass")
    field( PINI, "YES")
    field( VAL , "$(AcTrig-Bypass-Sel\=1)")
    field( UDF,  "0")
    field( ZNAM, "Off")
    field( ONAM, "On")
    field( FLNK, "$(SYS)-$(DEVICE):AcTrig-Bypass-RB")
    info( autosaveFields_pass0, "VAL")
}

record(bi, "$(SYS)-$(DEVICE):AcTrig-Bypass-RB") {
    field( DESC, "Bypass AC divider and Phase shifter")
    field( DTYP, "Obj Prop bool")
    field( INP,  "@OBJ=$(DEVICE):AcTrig, PROP=Bypass")
    field( ZNAM, "Off")
    field( ONAM, "On")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-SyncSrc-Sel") {
    field( DESC, "Sync to Event Clock or Mxc7")
    field( DTYP, "Obj Prop bool")
    field( OUT,  "@OBJ=$(DEVICE):AcTrig, PROP=SyncSrc")
    field( PINI, "YES")
    field( VAL , "$(AcTrig-SyncSrc-Sel\=1)")
    field( UDF,  "0")
    field( ZNAM, "Event Clk")
    field( ONAM, "Mxc7")
    field( FLNK, "$(SYS)-$(DEVICE):AcTrig-SyncSrc-RB")
    info( autosaveFields_pass0, "VAL")
}

record(bi, "$(SYS)-$(DEVICE):AcTrig-SyncSrc-RB") {
    field( DESC, "Sync to Event Clock or Mxc7")
    field( DTYP, "Obj Prop bool")
    field( INP,  "@OBJ=$(DEVICE):AcTrig, PROP=SyncSrc")
    field( ZNAM, "Event Clk")
    field( ONAM, "Mxc7")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc0-SP") {
    field( DESC, "Event trigger 0")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S0 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc0-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc1-SP") {
    field( DESC, "Event trigger 1")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S1 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc1-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc2-SP") {
    field( DESC, "Event trigger 2")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S2 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc2-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc3-SP") {
    field( DESC, "Event trigger 3")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S3 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc3-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc4-SP") {
    field( DESC, "Event trigger 4")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S4 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc4-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc5-SP") {
    field( DESC, "Event trigger 5")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S5 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc5-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc6-SP") {
    field( DESC, "Event trigger 6")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S6 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc6-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

record(bo, "$(SYS)-$(DEVICE):AcTrig-TrigSrc7-SP") {
    field( DESC, "Event trigger 7")
    field( DTYP, "EVG Trig Evt AC")
    field( OUT , "#C S7 @$(DEVICE):AcTrig")
    field( PINI, "YES")
    field( ZNAM, "Clear")
    field( ONAM, "Set")
    field( VAL, "$(AcTrig-TrigSrc7-SP\=0)")
    info( autosaveFields_pass0, "VAL")
}

